#!/bin/sh

PREPUSH_SCRIPT=hook--pre-push.sh
PREPUSH_FOLDER=devtools
PREPUSH_HOOK=pre-push

HOOKS_FOLDER=.git/hooks

PROJECT_NAME=pages-roses-backend

SRC_BASE_FOLDER=$(echo $PWD | sed -e "s/"$PROJECT_NAME".*//g")
PROJECT_ROOT_FOLDER=$SRC_BASE_FOLDER$PROJECT_NAME

cd $PROJECT_ROOT_FOLDER/$HOOKS_FOLDER

# 1. add shebang to hook script
echo "#!/bin/sh" > $PREPUSH_HOOK

# 2. add cd root to hook SCRIPT_TOOL_FOLDER to force pre-push execution in root folder
echo "cd $PROJECT_ROOT_FOLDER" >> $PREPUSH_HOOK

# # 3. add script execution to hook script
echo "sh $PROJECT_ROOT_FOLDER/$PREPUSH_FOLDER/$PREPUSH_SCRIPT" >> $PREPUSH_HOOK
chmod 755 $PREPUSH_HOOK

echo "Prepush hook install. Run git push --no-verify to ignore it."
