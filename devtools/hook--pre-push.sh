#!/bin/bash
# To use, store as .git/hooks/pre-commit inside your repository and make sure
# it has execute permissions.

MAGENTA="\033[35m"
GREEN="\033[32m"
BLUE="\033[96m"
RED="\033[91m"
YELLOW="\033[93m"
DEFAULT="\033[39m"

echo "$MAGENTA Ensure you are pushing from root folder of the repository.$DEFAULT"

commit_nb=$(git cherry -v | wc -l | awk '{print $1}')
echo "$BLUE[ -- You have trying to push $DEFAULT$commit_nb$BLUE commits, analyze in progress... -- ]$DEFAULT"

go_files=$(git diff --name-only --diff-filter=ACM HEAD~$commit_nb | grep ".*\.go\$" | sort | uniq)
if [ ! -z $go_files ]; then
  echo "-~-~-~-~-~-~-"
  echo "$BLUE 1. Build$DEFAULT"
  go build
  echo "$GREEN => Build OK.$DEFAULT"

  echo "-~-~-~-~-~-~-"
  echo "$BLUE 2. Code format$DEFAULT"
  invalid_format=`gofmt -l $go_files`
  if [ ! -z "$invalid_format" ]; then
    echo "$RED Invalid formatted files. Push aborted.\n $invalid_format$DEFAULT"
    exit 1
  fi
  echo "$GREEN => Format OK.$DEFAULT"

else
  echo "$BLUE 1. No go files has been changed.$DEFAULT"
  echo "$GREEN => Go files OK..$DEFAULT"
fi

echo "[__ $GREEN _o/\o_ $DEFAULT=>$BLUE Everything is awesome !$RED *\o/*$MAGENTA Push allowed. $GREEN _o/\o_ $DEFAULT __]"
