module gitlab.com/Misakey/pages-roses-backend

go 1.12

require (
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/lib/pq v1.2.0
	github.com/rs/zerolog v1.15.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.2.0+incompatible // indirect
	gitlab.com/Misakey/msk-sdk-go v0.0.0-20191025072618-05128997d0fb
)
