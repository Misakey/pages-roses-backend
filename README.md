# Introduction
Pages Roses is Backend for Frontend, fetch and merge applications stuff from `gitlab.com/Misakey/application-backend`.


## Table of content

* [Introduction](#introduction)
* [Folder Architecture](#folder-architecture)
* [Run and Develop](#run-and-develop)
  * [Dependencies](#dependencies)
    * [Docker](#docker)
  * [Configuration](#configuration)
    * [Environment](#environment)
    * [Configuration File](#configuration-file)
*[Deployment](#deployment)

------

## Folder architecture

_Main folders:_
- `config`: configuration files
- `devtools`: ci tools
- `docs`: documentation resources, API documentation...
- `src`: source code of the project.

_Concerning the source code:_

At the source level, each modules and future services aims to follow Clean Architecture principles ([here is a quick introduction](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)) and Layered Architecture.
Here is the list of folders and a description:
- `controller`: presenters layers, handling data transformation (usually from JSON to internal model)
- `service`: most of the business logic, interact with repo to play with models/data
- `repository`: handle retrieval/storage of models (can use a DB, an API...)
- `model`: our model, understandable and used by all our layers
- `adaptor`: code linked to frameworks and drivers we use to build our service, mostly for customisation purpose
- `cmd`: cobra commands

------

## Run and Develop

### Dependencies
##### Docker

The project is using [docker](https://www.docker.com/community-edition) to facilitate the run and the deployment.

### Configuration

Configuration is done on **1 level**:
- [a configuration file](#configuration-file) (we use [viper](https://github.com/spf13/viper#what-is-viper) so many possibilities in term of format)

##### Environment

Some environment variables are needed.

Here is the list of variable to set up to have the account service working properly:

- `ENV`: the environment mode you choose (`production` or `development`):
  - Use a different [configuration file](#configuration-file).


##### Configuration File

The service always tries to read the config file from the `/etc` folder.

In development mode, it tries to read `pages-roses-config.dev.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is automatically added to the built Docker image using `config/pages-roses-config.toml`.

In production mode, it tries to read `pages-roses-config.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is **NOT** automatically added to the built Docker image. A volume must be mounted to share it.

An example a of configuration file (using TOML) is available in the `config` folder.

## Deployment

:warning: Make sure the corresponding images exist before running the following commands.

Create a `config.yaml` file with a `config:` key and the `config.toml` production content.


When you want to deploy the application for the first time, clone the repo, checkout to the desired tag, then run:

```
helm install --name pages-roses-backend helm/pages-roses-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest

```

If you just need to upgrade the application, then run:
```
helm upgrade pages-roses-backend helm/pages-roses-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest
```

The command will hang during the migration. Check in another terminal if there is no problem with the migration.

If the migration fails, CTRL+C your helm command, then run `kubectl delete jobs pages-roses-backend-migrate`, then solve the problem and re-run the helm command.
