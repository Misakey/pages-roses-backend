package main

import (
	"fmt"
	"gitlab.com/Misakey/pages-roses-backend/src/cmd"
	"os"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
