package controller

import (
	"context"

	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

type serviceApplication interface {
	Read(ctx context.Context, mainDomain string) (*model.Application, error)
	Find(ctx context.Context, filters model.ApplicationFilters) ([]*model.Application, error)
}
