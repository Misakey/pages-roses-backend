package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/pages-roses-backend/src/adaptor/slices"
	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

type ApplicationEcho struct {
	service serviceApplication
}

// ApplicationEcho constructor
func NewApplicationEcho(service serviceApplication) *ApplicationEcho {
	return &ApplicationEcho{
		service: service,
	}
}

// ReadByID
func (c *ApplicationEcho) Read(ctx echo.Context) error {
	mainDomain := ctx.Param("main_domain")

	ai, err := c.service.Read(ctx.Request().Context(), mainDomain)
	if err != nil {
		return merror.Transform(err).Describe("could not read application")
	}
	return ctx.JSON(http.StatusOK, ai)
}

// List
func (c *ApplicationEcho) List(ctx echo.Context) error {
	filters := model.ApplicationFilters{}

	filters.Search = ctx.QueryParam("search")
	filters.MainDomain = ctx.QueryParam("main_domain")
	filters.IDs = slices.FromSep(ctx.QueryParam("ids"), ",")

	// If search param is not alone, return error
	if filters.Search != "" && (filters.MainDomain != "" || len(filters.IDs) > 0) {
		return merror.BadRequest().From(merror.OriQuery).Detail("for searching :'search' must be the only query param", merror.DVInvalid)
	}

	err := ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	apps, err := c.service.Find(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriInternal).Describe("could not list applications")
	}
	return ctx.JSON(http.StatusOK, apps)
}
