package cmd

import (
	"fmt"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/bubble"
	"gitlab.com/Misakey/msk-sdk-go/config"
	mecho "gitlab.com/Misakey/msk-sdk-go/echo"
	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/pages-roses-backend/src/controller"
	"gitlab.com/Misakey/pages-roses-backend/src/repo"
	"gitlab.com/Misakey/pages-roses-backend/src/service"
)

var cfgFile string
var env = os.Getenv("ENV")

func init() {
	cobra.OnInitialize()
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

var RootCmd = &cobra.Command{
	Use:   "pages-roses",
	Short: "Run the pages-roses backend for frontend",
	Long:  `This service is a backend for frontend for the main Misakey application`,
	Run: func(cmd *cobra.Command, args []string) {
		initService()
	},
}

func initService() {
	// init logger
	log.Logger = logger.ZerologLogger()

	initDefaultConfig()

	// add error needles to auto handle some specific errors on layers we use everywhere
	bubble.AddNeedle(bubble.PSQLNeedle{})
	bubble.AddNeedle(bubble.ValidatorNeedle{})
	bubble.AddNeedle(bubble.EchoNeedle{})
	bubble.Lock()

	e := echo.New()
	e.HideBanner = true

	e.Use(mecho.NewZerologLogger())
	e.Use(mecho.NewLogger())
	e.Use(mecho.NewCORS())
	e.Use(middleware.Recover())
	e.HTTPErrorHandler = mecho.Error
	e.Validator = mecho.NewValidator()

	appInfoUri := viper.GetString("application.endpoint")
	appFeedbackUri := viper.GetString("feedback.endpoint")

	jwtModerateMidlw := mecho.NewJWTMidlw(false)

	applicationRepo := repo.NewApplicationHTTP(appInfoUri)
	ratingRepo := repo.NewRatingHTTP(appFeedbackUri)
	applicationService := service.NewApplication(applicationRepo, ratingRepo)
	applicationController := controller.NewApplicationEcho(applicationService)

	// Init generic
	genericController := controller.NewGeneric()

	application := e.Group("applications")
	application.GET("", applicationController.List, jwtModerateMidlw)
	application.GET("/:main_domain", applicationController.Read, jwtModerateMidlw)

	// Bind generic routes
	generic := e.Group("")
	generic.GET("/version", genericController.Version)

	// launch echo server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("server.port"))))
}

func initDefaultConfig() {
	// always look inside the /etc folder for the configuration file
	viper.AddConfigPath("/etc/")

	// set defaults for configuration
	viper.SetDefault("server.port", 5000)

	// handle different envs
	switch env {
	case "production":
		viper.SetConfigName("pages-roses-config")
	case "development":
		viper.SetConfigName("pages-roses-config.dev")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// handle missing mandatory fields
	mandatoryFields := []string{
		"application.endpoint",
		"feedback.endpoint",
		"server.cors",
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)
	config.Print(nil)
}
