package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

type Application struct {
	application applicationRepo
	rating      *Rating
}

// Application constructor
func NewApplication(applicationRepo applicationRepo, ratingRepo ratingRepo) *Application {
	rating := NewRating(ratingRepo)
	return &Application{
		application: applicationRepo,
		rating:      rating,
	}
}

// Read using main domain
func (service *Application) Read(ctx context.Context, mainDomain string) (*model.Application, error) {
	filters := model.ApplicationFilters{}
	filters.MainDomain = mainDomain

	applications, err := service.application.ListApplicationInfo(ctx, filters)
	if err != nil {
		return nil, err
	}

	if len(applications) == 0 {
		return nil, merror.NotFound().Describef("application with domain %s not found", mainDomain)
	}
	application := applications[0]

	// retrieve application links
	var links []*model.Link
	types := []string{"home_page", "legal_notice", "privacy_policy"}
	links, err = service.application.ListLink(ctx, application.ID, types)
	if err != nil {
		return nil, err
	}
	application.Links = links

	// retrieve application linked domains
	var domains []*model.Domain
	domains, err = service.application.ListDomain(ctx, application.ID, 30, 0)
	if err != nil {
		return nil, err
	}
	application.Domains = domains

	err = service.rating.computeRating(ctx, application)
	return application, err
}

// Find
func (service *Application) Find(ctx context.Context, filters model.ApplicationFilters) ([]*model.Application, error) {
	applications, err := service.application.ListApplicationInfo(ctx, filters)
	if err != nil {
		return nil, err
	}

	if len(applications) == 0 {
		return applications, nil
	}

	for _, application := range applications {
		err := service.rating.computeRating(ctx, application)
		if err != nil {
			return applications, err
		}
	}

	return applications, err
}
