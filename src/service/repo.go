package service

import (
	"context"

	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

type applicationRepo interface {
	ListApplicationInfo(ctx context.Context, filters model.ApplicationFilters) ([]*model.Application, error)
	ListLink(ctx context.Context, applicationID string, types []string) ([]*model.Link, error)
	ListDomain(ctx context.Context, applicationID string, limit int, offset int) ([]*model.Domain, error)
}

type ratingRepo interface {
	ListRatings(ctx context.Context, appID string) ([]*model.Rating, error)
}
