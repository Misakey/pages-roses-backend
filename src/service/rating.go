package service

import (
	"context"
	"math"

	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

// RatingRepo is used to retrieve and compute application Ratings
type Rating struct {
	repo ratingRepo
}

// Rating constructor
func NewRating(ratingRepo ratingRepo) *Rating {
	return &Rating{
		repo: ratingRepo,
	}
}

// ComputeRating
func (service *Rating) computeRating(ctx context.Context, application *model.Application) error {
	ratings, err := service.repo.ListRatings(ctx, application.ID)
	// if no ratings return before compute average
	if err != nil {
		return err
	}
	if len(ratings) == 0 {
		return nil
	}

	var avgInt int16
	// Iter on all ratings to fill the rating histogram and compute the rating average
	for _, rating := range ratings {
		avgInt += rating.Value
		application.DistrRating[rating.Value-1] += 1
	}

	application.TotalRating = len(ratings)
	// Compute the Average rating and round .1 digit
	application.AvgRating = float64(avgInt) / float64(application.TotalRating)
	application.AvgRating = math.Round(application.AvgRating*10) / 10

	return nil
}
