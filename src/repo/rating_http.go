package repo

import (
	"context"
	"net/url"

	"gitlab.com/Misakey/pages-roses-backend/src/adaptor/rester/http"
	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

// RatingHTTP used for applications ratings from application api
type RatingHTTP struct {
	appFeedbackRester *http.Rester
}

// RatingHTTP constructor
func NewRatingHTTP(afu string) *RatingHTTP {
	return &RatingHTTP{
		appFeedbackRester: http.NewRester(afu, false),
	}
}

// ListRatings
func (rh *RatingHTTP) ListRatings(ctx context.Context, appID string) ([]*model.Rating, error) {
	var ratings []*model.Rating

	getUrl := "/ratings?application_id=" + url.PathEscape(appID)
	err := rh.appFeedbackRester.Get(ctx, getUrl, nil, &ratings)

	return ratings, err
}
