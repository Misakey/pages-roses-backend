package repo

import (
	"context"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/Misakey/pages-roses-backend/src/adaptor/rester/http"
	"gitlab.com/Misakey/pages-roses-backend/src/model"
)

// ApplicationHTTP used for applications info from application api
type ApplicationHTTP struct {
	appInfoRester *http.Rester
}

// ApplicationHTTP constructor
func NewApplicationHTTP(aiu string) *ApplicationHTTP {
	return &ApplicationHTTP{
		appInfoRester: http.NewRester(aiu, false),
	}
}

// ListApplicationInfo
func (ah *ApplicationHTTP) ListApplicationInfo(ctx context.Context, filters model.ApplicationFilters) ([]*model.Application, error) {
	var applications []*model.Application
	params := url.Values{}

	params.Set("ids", strings.Join(filters.IDs, ","))
	params.Set("main_domain", filters.MainDomain)
	params.Set("search", filters.Search)

	err := ah.appInfoRester.Get(ctx, "/application-info", params, &applications)

	return applications, err
}

// ListLink
func (ah *ApplicationHTTP) ListLink(ctx context.Context, applicationID string, types []string) ([]*model.Link, error) {
	var links []*model.Link

	params := url.Values{}

	params.Set("application_id", applicationID)
	params.Set("types", strings.Join(types, ","))

	err := ah.appInfoRester.Get(ctx, "/links", params, &links)

	return links, err
}

// ListDomain
func (ah *ApplicationHTTP) ListDomain(ctx context.Context, applicationID string, limit int, offset int) ([]*model.Domain, error) {
	var domains []*model.Domain

	params := url.Values{}

	params.Set("application_id", applicationID)
	params.Set("limit", strconv.Itoa(limit))
	params.Set("offset", strconv.Itoa(offset))

	err := ah.appInfoRester.Get(ctx, "/domains", params, &domains)

	return domains, err
}
