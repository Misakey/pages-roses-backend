package model

import "time"

type Rating struct {
	ID            int       `json:"id"`
	UserID        string    `json:"user_id"`
	ApplicationID string    `json:"application_id"`
	Value         int16     `json:"value"`
	CreatedAt     time.Time `json:"created_at"`
}
