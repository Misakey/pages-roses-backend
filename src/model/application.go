package model

import (
	"github.com/volatiletech/null"
)

// Filters for ApplicationInfo listing
type ApplicationFilters struct {
	IDs        []string `validate:"dive,uuid"`
	MainDomain string
	Search     string `validate:"omitempty,min=1"`
}

// Application model represents all the data in organization page
type Application struct {
	ID                 string      `json:"id"`
	Name               string      `json:"name"`
	MainDomain         string      `json:"main_domain"`
	LogoURI            null.String `json:"logo_uri"`
	ShortDesc          null.String `json:"short_desc"`
	LongDesc           null.String `json:"long_desc"`
	DpoEmail           null.String `json:"dpo_email"`
	MainColor          string      `json:"main_color"`
	ServiceType        null.String `json:"service_type"`
	PrivacyJuridiction null.String `json:"privacy_juridiction"`
	MainIncome         null.String `json:"main_income"`
	HomeContry         null.String `json:"home_country"`
	IsThirdParty       bool        `json:"is_third_party"`
	MainPurpose        string      `json:"main_purpose"`
	Links              []*Link     `json:"links"`
	Domains            []*Domain   `json:"domains"`
	AvgRating          float64     `json:"avg_rating"`
	TotalRating        int         `json:"total_rating"`
	DistrRating        [5]int      `json:"distr_rating"` // Used for build front rating histogram where DistrRating[0] == amount of rating value of 1 && DistrRating[4] == rating value of 5
}

// Link model represents a specific link of an application
type Link struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

// Domain model represents a domain linked to an application
type Domain struct {
	URI string `json:"uri"`
}
