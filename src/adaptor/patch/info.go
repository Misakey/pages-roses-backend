package patch

// Info contains fields to patch and model with patched data.
// See Map for more information.
type Info struct {
	Input  []string
	Output []string
	Model  interface{}
}

// Filter removes from Input fields given in parameters
// it also removes elements saved at same position in Output slice.
func (i *Info) Filter(removals []string) {
	filteredInput := []string{}
	filteredOutput := []string{}
	for pos, field := range i.Input {
		found := false
		for _, removal := range removals {
			if field == removal {
				found = true
				break
			}
		}
		if !found {
			filteredInput = append(filteredInput, field)
			filteredOutput = append(filteredOutput, i.Output[pos])
		}
	}
	i.Input = filteredInput
	i.Output = filteredOutput
}
