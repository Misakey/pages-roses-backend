package adaptor

import (
	"context"
	"fmt"
	"net/url"
)

// Rester represents an interface for REST clients.
type Rester interface {
	Get(ctx context.Context, route string, params url.Values, output interface{}) error
	Post(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error
	Put(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error
	Delete(ctx context.Context, route string, params url.Values) error
}

func BuildURL(secure bool, url string, route string, params url.Values) string {
	// configure protocol security
	protocol := "http"
	if secure {
		protocol = "https"
	}

	// build query string
	query := params.Encode()
	if len(query) > 0 {
		query = fmt.Sprintf("?%s", query)
	}

	return fmt.Sprintf("%s://%s%s%s", protocol, url, route, query)
}
