package http

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/pages-roses-backend/src/adaptor"
)

// Rester represents a HTTP REST API client requesting a configured endpoint.
type Rester struct {
	http.Client

	// host & port of targeted endpoint
	url string
	// optional usage of https
	secure bool
	// body response limit
	limit int64
}

const BLANK_FORMAT = ""
const JSON_FORMAT = "application/json"
const FORM_FORMAT = "application/x-www-form-urlencoded"

// NewRester is HTTP Rester constructor
func NewRester(url string, secure bool) *Rester {
	cli := &Rester{
		Client: http.Client{},
		url:    url,
		secure: secure,
		limit:  1024 * 1024,
	}
	return cli
}

// Get performs a GET request
func (r *Rester) Get(ctx context.Context, route string, params url.Values, output interface{}) error {
	return r.perform(ctx, "GET", route, params, nil, output, BLANK_FORMAT)
}

// Post performs a POST request, input is consiredered as form url encoded structure
func (r *Rester) Post(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error {
	return r.perform(ctx, "POST", route, params, input, output, JSON_FORMAT)
}

// Put performs a PUT request, input is consiredered as JSONable structure
func (r *Rester) Put(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error {
	return r.perform(ctx, "PUT", route, params, input, output, JSON_FORMAT)
}

// Delete performs a DELETE request
func (r *Rester) Delete(ctx context.Context, route string, params url.Values) error {
	return r.perform(ctx, "DELETE", route, params, nil, nil, BLANK_FORMAT)
}

func (r *Rester) perform(
	ctx context.Context,
	verb string,
	route string,
	params url.Values,
	input interface{},
	output interface{},
	format string,
) error {
	// 1. build URL, request
	req, err := http.NewRequest(verb, adaptor.BuildURL(r.secure, r.url, route, params), nil)
	if err != nil {
		return merror.Transform(err).Describe("could not create request")
	}
	// 2. use optionally input to fill body
	if input != nil {
		var data []byte
		data, err = json.Marshal(input)
		if err != nil {
			return merror.Transform(err).Describe("could not encode body")
		}
		req.Header.Set("Content-Type", format)
		req.ContentLength = int64(len(data))
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}

	// 3. set authorization header if possible with internal JWT
	acc := ajwt.GetAccesses(ctx)
	if acc != nil {
		req.Header.Set("Authorization", "Bearer "+acc.JWT)
	}

	// 4. perform request
	resp, err := r.Do(req)
	if err != nil {
		return merror.Transform(err).Describe("could not perform request")
	}

	return handleJSON(resp, output, r.limit)
}
