package http

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// handleJSON takes a http response and try to unmarshal it as JSON into given output interface
func handleJSON(resp *http.Response, output interface{}, limit int64) error {
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(io.LimitReader(resp.Body, limit))
	if err != nil {
		return merror.Transform(err).Describe("could not read response body")
	}

	// we consider an error occured below code 200 and above code 400
	if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusBadRequest {
		return merror.TransformHTTPCode(resp.StatusCode).Describe(string(data))
	}

	// if we were supposed to retrieve an output, we try to unmarshal it
	if output != nil {
		err = json.Unmarshal(data, output)
		if err != nil {
			desc := fmt.Sprintf("could not decode output: %v (%v)", err, strings.Replace(string(data), "\n", "", -1))
			return merror.Transform(err).Describe(desc)

		}
	}
	return nil
}
