# API Documentation

## Visualize API documentation:

`docker-compose up swagger_ui` then visit `localhost:7000` using a browser.

## Edit API documentation:

To edit the API Documentation:

- add the new route inside index.yaml
- if the new route is in a new group, create a yaml file according to the route name
- place the routes details and models definition inside the file
